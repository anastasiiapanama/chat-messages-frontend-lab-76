import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {NotificationContainer} from "react-notifications";

import App from './App';
import messagesReducer from "./store/reducers/messagesReducers";

import 'react-notifications/lib/notifications.css';

const rootReducer = combineReducers({
    messages: messagesReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

const app = (
    <Provider store={store}>
            <NotificationContainer/>
            <App/>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
