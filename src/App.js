import React from 'react';
import Messages from "./containers/Messages";

const App = () => <Messages />;

export default App;
