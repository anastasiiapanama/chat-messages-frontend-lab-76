import axiosApi from "../../axios-api";
import {NotificationManager} from "react-notifications";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';
export const FETCH_MESSAGES_BY_DATE = 'FETCH_MESSAGES_BY_DATE';

export const CHANGE_HANDLER_VALUE = 'CHANGE_HANDLER_VALUE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = payload => ({type: FETCH_MESSAGES_SUCCESS, payload});
export const fetchByDate = payload => ({type: FETCH_MESSAGES_BY_DATE, payload});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const changeHandlerValue = event => ({type: CHANGE_HANDLER_VALUE, event});

export const fetchMessages = () => {
    return async dispatch => {
        try {
            dispatch(fetchMessagesRequest());
            const response = await axiosApi.get('/messages');
            const lastDate = response.data[response.data.length - 1].datetime;
            dispatch(fetchMessagesSuccess({messages: response.data, lastDate}));
        } catch (e) {
            dispatch(fetchMessagesFailure());
            NotificationManager.error('Could not fetch messages');
        }
    };
};

export const addNewMessage = data => {
    return async dispatch => {
        try {
            const message = {
                message: data.messages.message,
                author: data.messages.author
            }
            await axiosApi.post('/messages', message);
        } catch (e) {
            NotificationManager.error('Could not fetch post message');
        }
    };
};

export const getLastDateMessages = date => {
    return async dispatch => {
        try {
            const response = await axiosApi.get(`/messages?datetime=${date}`);
            let lastDate
            if(response.data.length > 1) {
                lastDate = response.data[response.data.length - 1].datetime;
            } else {
                lastDate = response.data[0].datetime;
            }
            dispatch(fetchByDate({messages: response.data, lastDate}));
        } catch (e) {
            console.log(e);
        }
    };
};