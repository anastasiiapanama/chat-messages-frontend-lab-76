import {
    CHANGE_HANDLER_VALUE,
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS,
    FETCH_MESSAGES_BY_DATE
} from "../actions/messagesActions";

const initialState = {
    messages: [],
    message: '',
    author: '',
    datetime: '',
    messagesLoading: false
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_REQUEST:
            return {...state, messagesLoading: true};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messagesLoading: false, messages: action.payload.messages, datetime: action.payload.lastDate};
        case FETCH_MESSAGES_BY_DATE:
            console.log(action.payload);
            return {...state, messages: state.messages.concat(action.payload.messages), datetime: action.payload.lastDate}
        case FETCH_MESSAGES_FAILURE:
            return {...state, messagesLoading: false};
        case CHANGE_HANDLER_VALUE:
            return {...state, [action.event.name]: action.event.value};
        default:
            return state;
    }
};

export default messagesReducer;