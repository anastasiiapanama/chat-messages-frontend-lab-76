import React, {useEffect} from 'react';
import Post from "../components/Post/Post";

import {makeStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import TextField from "@material-ui/core/TextField";
import {useDispatch, useSelector} from "react-redux";
import {addNewMessage, changeHandlerValue, fetchMessages, getLastDateMessages} from "../store/actions/messagesActions";
import CircularProgress from "@material-ui/core/CircularProgress";
import Form from "../components/Form/Form";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    chatSection: {
        width: '100%',
        height: '100vh'
    },
    headBG: {
        backgroundColor: '#e0e0e0'
    },
    borderRight500: {
        borderRight: '1px solid #e0e0e0'
    },
    messageArea: {
        height: '70vh',
        overflowY: 'auto'
    },
    progress: {
        height: 200
    }
});

const Messages = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const state = useSelector(state => state);
    const lastDate = useSelector(state => state.messages.datetime);
    let interval = null;

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    useEffect(() => {
        if(lastDate) {
            interval = setInterval( () => {
                dispatch(getLastDateMessages(lastDate));
            }, 2000);
        };

        return () => clearInterval(interval)
    }, [dispatch, lastDate]);

    const onChangeMessage = e => {
        dispatch(changeHandlerValue(e.target));
    };

    const sendMessage = event => {
        event.preventDefault();

        dispatch(addNewMessage(state));
    };

    return (
        <>
            <Grid container>
                <Grid item xs={12}>
                    <Typography variant="h5" className="header-message">Chat</Typography>
                </Grid>
            </Grid>
            <Grid container component={Paper} className={classes.chatSection}>
                <Grid item xs={3} className={classes.borderRight500}>
                    <List>
                        <ListItem button key="Anastasiia">
                            <ListItemIcon>
                                <Avatar alt="Anastasiia" src="https://material-ui.com/static/images/avatar/3.jpg"/>
                            </ListItemIcon>
                            <ListItemText primary="Anastasiia"></ListItemText>
                        </ListItem>
                    </List>
                    <Divider/>
                    <Grid item xs={12} style={{padding: '10px'}}>
                        <TextField id="outlined-basic-email" label="Search" variant="outlined" fullWidth/>
                    </Grid>
                    <Divider/>
                    <List>
                        <ListItem button key="RemySharp">
                            <ListItemIcon>
                                <Avatar alt="Remy Sharp" src="https://material-ui.com/static/images/avatar/1.jpg"/>
                            </ListItemIcon>
                            <ListItemText primary="Remy Sharp">Remy Sharp</ListItemText>
                            <ListItemText secondary="online" align="right"></ListItemText>
                        </ListItem>
                        <ListItem button key="Alice">
                            <ListItemIcon>
                                <Avatar alt="Alice" src="https://material-ui.com/static/images/avatar/3.jpg"/>
                            </ListItemIcon>
                            <ListItemText primary="Alice">Alice</ListItemText>
                        </ListItem>
                        <ListItem button key="CindyBaker">
                            <ListItemIcon>
                                <Avatar alt="Cindy Baker" src="https://material-ui.com/static/images/avatar/2.jpg"/>
                            </ListItemIcon>
                            <ListItemText primary="Cindy Baker">Cindy Baker</ListItemText>
                        </ListItem>
                    </List>
                </Grid>
                <Grid item xs={9}>
                    <List className={classes.messageArea}>
                        {state.loading ? (
                            <Grid container justify="center" alignItems="center" className={classes.progress}>
                                <Grid item>
                                    <CircularProgress />
                                </Grid>
                            </Grid>
                        ) : state.messages.messages.map(message => {
                            return (
                                <Post
                                    key={message.id}
                                    message={message.message}
                                    author={message.author}
                                    datetime={message.datetime}
                                />
                            )
                        })}
                    </List>
                    <Divider/>
                    <Form
                        changeMessage={event => onChangeMessage(event)}
                        message={state.message}
                        author={state.author}
                        sendHandler={sendMessage}
                    />
                </Grid>
            </Grid>
        </>
    );
};

export default Messages;