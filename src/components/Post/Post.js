import React from 'react';
import Grid from "@material-ui/core/Grid";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(1),
        margin: theme.spacing(2)
    }
}));

const Post = ({message, author, datetime}) => {
    const classes = useStyles();

    return (
        <Paper className={classes.paper}>
            <ListItem key="2">
                <Grid container>
                    <Grid item xs={12}>
                        <ListItemText align="left" primary={message}></ListItemText>
                    </Grid>
                    <Grid item xs={12}>
                        <ListItemText align="left" secondary={author}></ListItemText>
                        <ListItemText align="left" secondary={datetime}></ListItemText>
                    </Grid>
                </Grid>
            </ListItem>
        </Paper>
    );
};

export default Post;