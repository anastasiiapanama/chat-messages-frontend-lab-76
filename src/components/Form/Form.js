import React from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Fab from "@material-ui/core/Fab";
import SendIcon from '@material-ui/icons/Send';

const Form = props => {
    return (
        <Grid container style={{padding: '20px'}}>
            <Grid item xs={11}>
                <TextField id="outlined-basic-email" label="Type Something"
                           name="message" value={props.message}
                           onChange={props.changeMessage}
                           fullWidth
                />
                <TextField id="outlined-basic-email" label="Author"
                           name="author" value={props.author}
                           onChange={props.changeMessage}
                           fullWidth
                />
            </Grid>
            <Grid xs={1} align="right">
                <Fab color="primary" aria-label="add"
                     onClick={props.sendHandler}
                >
                    <SendIcon />
                </Fab>
            </Grid>
        </Grid>
    );
};

export default Form;